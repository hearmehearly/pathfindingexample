﻿using System;


public class ScenePlayer
{
    public void Show(Action OnHidedCallback) =>
        UiMenuScreen.Instance.Show(() => OnHidedCallback?.Invoke());
}