﻿using System;


public class SceneArena
{
    private Action onFinishArena;


    public void Play(IArena arenaToPlay, Action onFinish)
    {
        onFinishArena = onFinish;
        arenaToPlay.ShowArena(OnFinishArena);
    }


    private void OnFinishArena() =>
        onFinishArena();
}
