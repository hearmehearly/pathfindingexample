﻿using System;
using UnityEngine;
using Object = UnityEngine.Object;


public class Arena : IArena
{
	private readonly UnitSettings unitSettings;
	private readonly ArenaSettings arenaSettings;
	private readonly Transform arenaFieldTransform;

	private readonly ArenaField arenaField;
	private static UnitArenaController unitArenaController;

	private Action hidedCallback;

	// TODO: should create levels controllers holder, and we should  get controllers from holder, but did static to save time
	public static UnitArenaController UnitArenaController =>
		unitArenaController;


	public Arena(UnitSettings _unitSettings, ArenaSettings _arenaSettings, Transform _arenaFieldTransform)
	{
		unitSettings = _unitSettings;
		arenaSettings = _arenaSettings;
		arenaFieldTransform = _arenaFieldTransform;

		arenaField = new ArenaField();
		unitArenaController = new UnitArenaController(arenaField);
	}


    public void ShowArena(Action onHided)
	{
		hidedCallback = onHided;

		unitArenaController.DestroyAllUnits(); // Do so to show winners after fight and destroy 'em only on next game start

		arenaField.SetupArena(arenaSettings);
		arenaField.Initialize();

		CreateUnit(Unit.Type.Friend);
		CreateUnit(Unit.Type.Enemy);

		unitArenaController.Initialize();
		unitArenaController.OnUnitDefeated += OnUnitDefeated;

		// todo: Should create content entity which controls objects creating/destroying
		// (plus, in that entity we can add object pooling, but i did not do it to save time)
		void CreateUnit(Unit.Type type)
		{
			(int x, int y)[] positions = unitSettings.GetSpawnCellRandomPositions(type);

			foreach (var (x, y) in positions)
			{
				var unit = Object.Instantiate(unitSettings.unitPrefab, arenaFieldTransform);
				unit.transform.localPosition = new Vector3(x, y, 0);
				unit.SetupSettings(unitSettings);
				unit.SetupType(type);

				unitArenaController.Add(unit);
			}
		}
	}


    private void Hided()
	{
		arenaField.Deinitialize();

		unitArenaController.OnUnitDefeated -= OnUnitDefeated;
		unitArenaController.Deinitialize();

		hidedCallback?.Invoke();
	}


	private void OnUnitDefeated(Unit.Type unitType)
	{
		bool isUnitsTypeAlive = unitArenaController.IsAnyUnitAlive(unitType);

		if (!isUnitsTypeAlive)
		{
			Hided();
		}
	}
}
