﻿using System;
using UnityEngine;


public class Game : MonoBehaviour
{
	public static event Action OnUpdate;

	// TODO: create static/singleton which has access for all settings and get settings from there.
	// To save time, I just made a direct link and set it with hierarchy
	[SerializeField] private ArenaSettings arenaSettings = default;
	[SerializeField] private UnitSettings unitSettings = default;
	[SerializeField] private Transform arenaFieldTransform = default;

    private SceneArena sceneArena;
    private ScenePlayer scenePlayer;
    private IArena arena;


	// This is the entry point. No Monobehaviour's methods should exist, except this class
    private void Awake()
	{
        sceneArena = new SceneArena();
        scenePlayer = new ScenePlayer();

		arena = new Arena(unitSettings, arenaSettings, arenaFieldTransform);

		ShowScenePlayer();
	}


    private void Update() =>
		OnUpdate?.Invoke();
    

    private void ShowArena() =>
		sceneArena.Play(arena, ShowScenePlayer);
    

    private void ShowScenePlayer() =>
		scenePlayer.Show(ShowArena);
}
