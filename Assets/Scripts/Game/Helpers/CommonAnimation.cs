﻿using System.Collections;
using System;
using UnityEngine;


[Serializable]
public abstract class CommonAnimation<T> where T: new()
{
    public delegate void OnFinish();

    public T begin = default;
    public T end = default;
    public float duration = 1.0f;

    private Coroutine routine;


    public CommonAnimation(T _begin = default, T _end = default, float _duration = 1.0f)
    {
        begin = _begin;
        end = _end;
        duration = _duration;
    }



    public void Stop(MonoBehaviour handler)
    {
        if (routine != null)
        {
            handler.StopCoroutine(routine);
        }
    }


    public void Play(Action<T> onValueChanged, MonoBehaviour handler, OnFinish onFinish = null, bool forward = true)
    {
        if (routine != null)
        {
            handler.StopCoroutine(routine);
        }

        routine = handler.StartCoroutine(Change(onValueChanged, onFinish, forward));
    }

    protected abstract T GetValue(float delta);

    private IEnumerator Change(Action<T> onValueChanged, OnFinish onFinish, bool forward)
    {
        float time = 0.0f;

        while (true)
        {
            if (time < duration)
            {
                time += Time.deltaTime;
                float delta = forward ? time / duration : (1 - time / duration);

                T value = GetValue(delta);
                onValueChanged?.Invoke(value);
            }
            else
            {
                T value = forward ? end : begin;
                onValueChanged?.Invoke(value);

                routine = null;
                onFinish?.Invoke();
                yield break;
            }

            yield return null;
        }
    }
}
