﻿using System;
using UnityEngine;


[Serializable]
public class VectorAnimation : CommonAnimation<Vector3>
{
    protected override Vector3 GetValue(float delta) =>
        Vector3.Lerp(begin, end, delta);


    public VectorAnimation(Vector3 _begin = default, Vector3 _end = default, float _duration = 1.0f) :
        base(_begin, _end, _duration)
    { }
}