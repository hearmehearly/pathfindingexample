﻿using System;
using UnityEngine;


[Serializable]
public class FloatAnimation : CommonAnimation<float>
{
    protected override float GetValue(float delta) =>
        Mathf.Lerp(begin, end, delta);

    public FloatAnimation(float _begin = default, float _end = default, float _duration = 1.0f) :
        base(_begin, _end, _duration)
    { }
}
