﻿using System;
using System.Collections.Generic;
using System.Linq;


public class UnitArenaController : ICommonBehaviour
{
    public event Action<Unit.Type> OnUnitDefeated;

    private readonly ArenaField arenaField;
    private readonly List<Unit> allUnitsOnMap = new List<Unit>();


    public UnitArenaController(ArenaField _arenaField)
    {
        arenaField = _arenaField;
    }


    public void Initialize()
    {
        foreach (var unit in allUnitsOnMap)
        {
            unit.StartGame(arenaField);
        }

        UnitHealthComponent.OnShouldDefeatUnit += OnShouldDefeatUnit;
    }


    public void Deinitialize()
    {
        UnitHealthComponent.OnShouldDefeatUnit -= OnShouldDefeatUnit;

        foreach (var unit in allUnitsOnMap)
        {
            unit.FinishGame();
        }
    }


    public void DestroyAllUnits()
    {
        foreach (var unit in allUnitsOnMap)
        {
            unit.FinishGame();
            UnityEngine.Object.Destroy(unit.gameObject);
        }

        allUnitsOnMap.Clear();
    }


    public Unit[] GetAllAliveUnitsToAttack(Unit.Type currentType) =>
        GetAllAliveUnits().Where(e => e.UnitType != currentType).ToArray();


    public Unit[] GetAllAliveUnits() =>
        allUnitsOnMap.ToArray();


    public bool IsAnyUnitAlive(Unit.Type type) =>
        GetAllAliveUnits().Where(e => e.UnitType == type && !e.IsHitted).Any();


    public void Add(Unit _unit) =>
        allUnitsOnMap.Add(_unit);


    private void OnShouldDefeatUnit(Unit unit)
    {
        unit.FinishGame();
        UnityEngine.Object.Destroy(unit.gameObject);
        allUnitsOnMap.Remove(unit);

        OnUnitDefeated?.Invoke(unit.UnitType);
    }
}
