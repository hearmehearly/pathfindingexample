﻿using System;


public  static class ArenaFieldExtensions
{
	public static void VisitNeighbourCells(this (int x, int y) cellPosition, Action<int, int> callback)
	{
		for (int x = cellPosition.x - 1; x <= cellPosition.x + 1; x++)
		{
			for (int y = cellPosition.y + 1; y >= cellPosition.y - 1; y--)
			{
				bool isCenter = cellPosition.x == x && cellPosition.y == y;
				if (!isCenter)
				{
					callback?.Invoke(x, y);
				}
			}
		}
	}
}
