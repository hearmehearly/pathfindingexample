﻿using System.Collections.Generic;


public class ArenaField : ICommonBehaviour
{
	private readonly List<(int x, int y)> lockedCells = new List<(int, int)>();

	private ArenaSettings arenaSettings;

	public void SetupArena(ArenaSettings _arenaSettings) =>
		arenaSettings = _arenaSettings;


	public bool CanMoveToCell((int x, int y) cell)
	{
		bool isCellFree = !lockedCells.Exists(e => e.x == cell.x && e.y == cell.y);

		return isCellFree &&
			cell.x >= 0 &&
			cell.x < arenaSettings.FieldMatrixLengthX &&
			cell.y >= 0 &&
			cell.y < arenaSettings.FieldMatrixLengthY;
	}


	public void Initialize()
	{
		UnitMoveComponent.OnShouldLockMovePosition += OnShouldLockMovePosition;
		UnitMoveComponent.OnShouldUnlockMovePosition += OnShouldUnlockMovePosition;
	}


	public void Deinitialize()
	{
		UnitMoveComponent.OnShouldLockMovePosition -= OnShouldLockMovePosition;
		UnitMoveComponent.OnShouldUnlockMovePosition -= OnShouldUnlockMovePosition;

		lockedCells.Clear();
	}


	private void OnShouldUnlockMovePosition((int x, int y) cell) =>
		lockedCells.RemoveAll(e => e.x == cell.x && e.y == cell.y);


	private void OnShouldLockMovePosition((int x, int y) cell) =>
		lockedCells.Add(cell);
}
