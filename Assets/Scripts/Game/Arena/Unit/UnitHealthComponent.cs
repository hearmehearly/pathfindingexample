﻿using System;
using UnityEngine;


public class UnitHealthComponent : UnitComponent
{
	public static event Action<Unit> OnShouldDefeatUnit;

	private readonly SpriteRenderer healthSlider;
	private readonly float initialHealthSliderValue;
	private readonly FloatAnimation sliderAnimation;

	private float health;


	public UnitHealthComponent(Unit _unit, UnitSettings _unitSettings, SpriteRenderer _healthSlider) :
		base(_unit, _unitSettings)
	{
		healthSlider = _healthSlider;

		initialHealthSliderValue = healthSlider.size.x;
		sliderAnimation = new FloatAnimation(unitSettings.healthSliderAnimation.begin,
											 unitSettings.healthSliderAnimation.end,
											 unitSettings.healthSliderAnimation.duration);
	}


	public override void Initialize()
	{
		health = unitSettings.health;
		UnitAttackComponent.OnShouldApplyDamage += OnShouldApplyDamage;

		healthSlider.size = new Vector2(initialHealthSliderValue, healthSlider.size.y);
	}


	public override void Deinitialize()
	{
		sliderAnimation.Stop(unit);
		UnitAttackComponent.OnShouldApplyDamage -= OnShouldApplyDamage;
	}


	private void OnShouldApplyDamage(Unit unitToApplyDamage)
	{
		if (unit == unitToApplyDamage)
		{
			health -= unitSettings.Damage;
			RunSliderTween();

			if (health <= 0.0f)
			{
				OnShouldDefeatUnit?.Invoke(unit);
			}
		}
	}


	// TODO: it's better to use DOTween.
	private void RunSliderTween()
	{
		sliderAnimation.Stop(unit);
		
		float endValue = health > 0.0f ? (health / unitSettings.health) * initialHealthSliderValue : default;

		sliderAnimation.begin = healthSlider.size.x;
		sliderAnimation.end = endValue;
		sliderAnimation.Play((value) => healthSlider.size = new Vector2(value, healthSlider.size.y), unit);
	}
}
