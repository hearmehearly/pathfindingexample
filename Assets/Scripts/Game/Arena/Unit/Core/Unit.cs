﻿using UnityEngine;
using System.Collections.Generic;


public class Unit : MonoBehaviour
{
	public enum Type
	{
		None = 0,
		Friend = 1,
		Enemy = 2
	}

	[SerializeField] private SpriteRenderer spriteRenderer = default;
	[SerializeField] private SpriteRenderer healthSlider = default;

	private UnitSettings unitSettings;

	private List<UnitComponent> components;

	public bool IsHitted { get; private set; }
	public Type UnitType { get; private set; }

	public (int x, int y) CurrentCellPosition =>
		((int)transform.localPosition.x, (int)transform.localPosition.y);


	public void SetupType(Type _type)
	{ 
		UnitType = _type;
		spriteRenderer.sprite = unitSettings.FindUnitVisualSprite(UnitType);
		healthSlider.color = unitSettings.FindUnitHealthSliderColor(UnitType);
	}


	public void SetupSettings(UnitSettings _unitSettings) =>
		unitSettings = _unitSettings;


	public void StartGame(ArenaField arenaField)
	{
		components = components ?? new List<UnitComponent>()
		{
			new UnitAttackComponent(this, unitSettings),
			new UnitMoveComponent(this, arenaField, unitSettings),

			//TODO: Can be splitted on visual and health monitor component.
			new UnitHealthComponent(this, unitSettings, healthSlider)
		};

		foreach (var component in components)
		{
			component.Initialize();
		}

		IsHitted = false;
	}


	public void FinishGame()
	{
		foreach (var component in components)
		{
			component.Deinitialize();
		}
	}


	public void MarkHitted() =>
		IsHitted = true;
}
