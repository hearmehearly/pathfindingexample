﻿public abstract class UnitComponent : ICommonBehaviour
{
	protected readonly Unit unit;
	protected readonly UnitSettings unitSettings;


	public UnitComponent(Unit _unit, UnitSettings _unitSettings)
	{
		unit = _unit;
		unitSettings = _unitSettings;
	}

	public abstract void Initialize();

	public abstract void Deinitialize();
}
