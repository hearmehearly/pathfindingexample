﻿using System;
using System.Collections;
using UnityEngine;


public class UnitAttackComponent : UnitComponent
{
	public static event Action<Unit> OnShouldApplyDamage;

	private Coroutine attackRoutine;


	public UnitAttackComponent(Unit _unit, UnitSettings _unitSettings) :
		base(_unit, _unitSettings)
	{
	}


	public override void Initialize()
	{
        UnitMoveComponent.OnMovementStopped += OnMovementStopped;
	}


    public override void Deinitialize()
	{
		UnitMoveComponent.OnMovementStopped -= OnMovementStopped;

		if (attackRoutine != null)
		{
			unit.StopCoroutine(attackRoutine);
			attackRoutine = null;
		}
	}


	private void OnMovementStopped(Unit anotherUnit, Unit unitToAttack)
	{
		if (unit == anotherUnit)
		{
			AttemptToAttack(unitToAttack);
		}
	}


	private void AttemptToAttack(Unit unitToAttack)
	{
		if (attackRoutine == null)
		{
			attackRoutine = unit.StartCoroutine(AttackAction());
		}

		IEnumerator AttackAction()
		{
			yield return new WaitForSeconds(unitSettings.attackDelay);

			attackRoutine = null;
			OnShouldApplyDamage?.Invoke(unitToAttack);
		}
	}
}
