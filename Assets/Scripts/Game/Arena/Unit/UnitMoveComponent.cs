﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;


public class UnitMoveComponent : UnitComponent
{
	public static event Action<Unit, Unit> OnMovementStopped; // current, another

	public static event Action<(int, int)> OnShouldLockMovePosition;
	public static event Action<(int, int)> OnShouldUnlockMovePosition;
	 
	private readonly ArenaField arenaField;
	private readonly VectorAnimation moveAnimation;

	private Coroutine moveRoutine;
	private Unit[] allUnitsToAttack;

	private (int x, int y) lastMoveToCellPosition;



	public UnitMoveComponent(Unit _unit, ArenaField _arenaField, UnitSettings _unitSettings) :
		base(_unit, _unitSettings)
	{
		arenaField = _arenaField;

		moveAnimation = new VectorAnimation(unitSettings.moveAnimation.begin,
											unitSettings.moveAnimation.end,
											unitSettings.moveAnimation.duration);
	}


	public override void Initialize()
	{
		allUnitsToAttack = Arena.UnitArenaController.GetAllAliveUnitsToAttack(unit.UnitType);
		lastMoveToCellPosition = unit.CurrentCellPosition;

		OnShouldLockMovePosition?.Invoke(unit.CurrentCellPosition);

		Game.OnUpdate += UpdateMovement;
        UnitHealthComponent.OnShouldDefeatUnit += OnShouldDefeatUnit;
	}


    public override void Deinitialize()
	{
		UnitHealthComponent.OnShouldDefeatUnit -= OnShouldDefeatUnit;
		Game.OnUpdate -= UpdateMovement;

        //moveAnimation.Stop(unit);

		if (moveRoutine != null)
		{
			unit.StopCoroutine(moveRoutine);
		}
	}

	// Fast logic, that recalculate every frame just closest neighbour cell to closest enemy
	// Might be not perfect, but easy to perfom and to understand.
	// Actually, maybe it's better to create one controller, which can go for loop in all units and calculate all positions.
	private void UpdateMovement()
	{
		bool isClosestEnemyExists = TryFindClosestEnemy(out Unit closestEnemy);

		if (isClosestEnemyExists)
		{
			if (IsEnemyInNeighbourCell(closestEnemy.CurrentCellPosition))
			{
				OnMovementStopped?.Invoke(unit, closestEnemy);
			}
			else
			{
				moveRoutine = unit.StartCoroutine(MoveToNearestNeighboursCell(closestEnemy));
			}
		}
	}


	private IEnumerator MoveToNearestNeighboursCell(Unit unitMoveTo)
	{
		Game.OnUpdate -= UpdateMovement;

		List<(float distance, int x, int y)> neighborsDistances = new List<(float, int, int)>();

		(int x, int y) moveBeginPosition = unit.CurrentCellPosition;

		float currentDistance = Vector3.Distance(unit.transform.position, unitMoveTo.transform.position);
		neighborsDistances.Add((currentDistance, moveBeginPosition.x, moveBeginPosition.y));

		moveBeginPosition.VisitNeighbourCells((x, y) =>
		{
			bool canMoveToCell = arenaField.CanMoveToCell((x, y));

			if (canMoveToCell)
			{
				float distanceToEnemy = Vector3.Distance(new Vector3(x, y, 0.0f), new Vector3(unitMoveTo.CurrentCellPosition.x, unitMoveTo.CurrentCellPosition.y, 0.0f));
				neighborsDistances.Add((distanceToEnemy, x, y));
			}
		});

		(int x, int y) nearestNeighbor = neighborsDistances.OrderBy(e => e.distance)
														   .Select(e => (e.x, e.y))
														   .FirstOrDefault();

		Vector3 moveEndPosition = new Vector3(nearestNeighbor.x, nearestNeighbor.y, 0);

		bool isSameCell = moveBeginPosition.x == nearestNeighbor.x &&
						  moveBeginPosition.y == nearestNeighbor.y;

		if (isSameCell)
		{
			Game.OnUpdate += UpdateMovement;
			moveRoutine = null;
			yield break;
		}

		lastMoveToCellPosition = nearestNeighbor;

		OnShouldLockMovePosition?.Invoke(nearestNeighbor);

        yield return new WaitForSeconds(unitSettings.unitMoveDelay);

		OnShouldUnlockMovePosition?.Invoke(moveBeginPosition);

		moveAnimation.Stop(unit);
		moveAnimation.begin = unit.transform.localPosition;
		moveAnimation.end = moveEndPosition;
		moveAnimation.Play((value) => unit.transform.localPosition = value, unit);

		yield return new WaitForSeconds(unitSettings.unitMoveDuration);

		Game.OnUpdate += UpdateMovement;
		moveRoutine = null;
	}


	private bool IsEnemyInNeighbourCell((int x, int y) enemyCellPosition)
	{
		bool result = false;

		unit.CurrentCellPosition.VisitNeighbourCells((x, y) =>
		{
			if (x == enemyCellPosition.x && y == enemyCellPosition.y)
			{
				result = true;
			}
		});

		return result;
	}


	private bool TryFindClosestEnemy(out Unit closestUnit)
	{
		// todo; should get event and remove from list by event, but do fast to save time
		closestUnit = allUnitsToAttack.Where(e => e != null)
									  .OrderBy(e => Vector3.Distance(e.transform.position, unit.transform.position))
									  .FirstOrDefault();
		return closestUnit != null;
	}


	private void OnShouldDefeatUnit(Unit anotherUnit)
	{
		if (unit == anotherUnit)
		{
            OnShouldUnlockMovePosition?.Invoke(lastMoveToCellPosition);
        }
	}
}
