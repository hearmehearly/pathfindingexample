﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;


public class UnitSettings : ScriptableObject
{
    // TODO: It's better to separate data for certain ScriptableObject's (Unit Settings and Content Settings etc), but do everything in one to save time.

    [Serializable]
    private class UnitData
    {
        public Unit.Type type = default;

        public Sprite visual = default;
        public Color healthSliderColor = default;

        public int minCount = default;
        public int maxCount = default;

        [Header("Spawn Settings")]
        // TODO: Range should depends on some conditions, but do so to save time as we know map size (6x8)
        [Range(0, 5)] public int minRangeX = default;
        [Range(0, 5)] public int maxRangeX = default;

        [Range(0, 7)] public int minRangeY = default;
        [Range(0, 7)] public int maxRangeY = default;
    }

    public Unit unitPrefab = default;

    [Header("Animations")]
    public VectorAnimation moveAnimation = default;
    public FloatAnimation healthSliderAnimation = default;
    public float unitMoveDelay = default;
    public float unitMoveDuration = default;

    [Header("Data")]
    [SerializeField] private UnitData[] unitData = default;

    [Header("Common Settings")]
    public float attackDelay = default;
    public float health = default;
    [SerializeField] private float minDamage = default;
    [SerializeField] private float maxDamage = default;


    public float Damage =>
        Random.Range(minDamage, maxDamage);


    public (int, int)[] GetSpawnCellRandomPositions(Unit.Type type)
    {
        var foundData = FindData(type);

        if (foundData.maxRangeX < foundData.minRangeX ||
            foundData.maxRangeY < foundData.minRangeY)
        {
            Debug.Log($"Wrong data set for {type} in {this}");
            return Array.Empty<(int, int)>();
        }

        int count = GetRandomUnitsCountForField(type);
        List<(int, int)> result = new List<(int, int)>(count);

        List<(int, int)> allowedPositions = new List<(int, int)>(foundData.maxRangeX * foundData.maxRangeY);
        FillAllowedPositions();

        for (int i = 0; i < count; i++)
        {
            bool isOverFlow = allowedPositions.Count == 0;
            if (isOverFlow)
            {
                Debug.Log($"Not enough positions to fill {count} units if {type} type");
                break;
            }

            (int, int) cellPosition = allowedPositions[Random.Range(0, allowedPositions.Count)];
            result.Add(cellPosition);

            allowedPositions.Remove(cellPosition);
        }

        return result.ToArray();


        void FillAllowedPositions()
        {
            List<int> allowedPositionsX = new List<int>(foundData.maxRangeX - foundData.minRangeX);
            for (int value = foundData.minRangeX; value <= foundData.maxRangeX; value++)
            {
                allowedPositionsX.Add(value);
            }

            foreach (var allowedX in allowedPositionsX)
            {
                for (int y = foundData.minRangeY; y <= foundData.maxRangeY; y++)
                {
                    allowedPositions.Add((allowedX, y));
                }
            }
        }
    }


    public Sprite FindUnitVisualSprite(Unit.Type type)
    {
        var foundData = FindData(type);
        return foundData == null ? default : foundData.visual;
    }


    public Color FindUnitHealthSliderColor(Unit.Type type)
    {
        var foundData = FindData(type);
        return foundData == null ? Color.white : foundData.healthSliderColor;
    }
    

    private int GetRandomUnitsCountForField(Unit.Type type)
    {
        var foundData = FindData(type);

        if (foundData.maxCount < foundData.minCount)
        {
            Debug.Log($"Wrong data set for {type} in {this}");
            return foundData.minCount;
        }

        int randomCount = foundData == null ? default : Random.Range(foundData.minCount, foundData.maxCount + 1);
        return randomCount;
    }


    private UnitData FindData(Unit.Type type)
    {
        var foundData = Array.Find(unitData, e => e.type == type);
        if (foundData == null)
        {
            Debug.Log($"No data found for {type} in {this}");
        }

        return foundData;
    }
}
