﻿using UnityEngine;


public class ArenaSettings : ScriptableObject
{
    // TODO: Actually, we can pass x and y and generate correct visual field for all sizes. But did it hardcoded 6x8 to save the time.
    public int FieldMatrixLengthX => 6;
    public int FieldMatrixLengthY => 8;
}
