﻿using System;
using UnityEngine;
using UnityEngine.UI;


public class UiMenuScreen : MonoBehaviour
{
    // TODO: Use unity animator, but do simple color animation to save the time

    [Header("Animation")]
    [SerializeField] private CanvasGroup canvasGroup = default;
    [SerializeField] private FloatAnimation buttonHideAnimation = default;

    [Header("Data")]
    [SerializeField] private Button playButton = default;

    private Action hidedCallback;


    // TODO: Create good ui manager, which can handle all screens, but do so to save the time.
    public static UiMenuScreen Instance { get; private set; }


    private void Awake()
    {
        if (Instance != null)
        {
            Debug.LogError($"More than one Singleton's for {this} has been found.");
        }

        Instance = this;
    }


    public void Show(Action onHided)
    {
        hidedCallback = onHided;
        DeinitializeButtons();

        buttonHideAnimation.Play((value) => canvasGroup.alpha = value, this, InitializeButtons, false);
    }


    private void Hide()
    {
        DeinitializeButtons();
        buttonHideAnimation.Play((value) => canvasGroup.alpha = value, this, () => hidedCallback?.Invoke());
    }


    private void InitializeButtons() =>
        playButton.onClick.AddListener(Hide);


    private void DeinitializeButtons() =>
        playButton.onClick.RemoveListener(Hide);
}
